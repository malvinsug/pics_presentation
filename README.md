# PICS_Presentation

writing tex file with Git for better workflow.

# Latex + Git Workflow

Copied from https://stackoverflow.com/questions/6188780/git-latex-workflow.

Changes to your LaTeX workflow:

The first step in efficiently managing a Git+LaTeX workflow is to make a few changes to your LaTeX habits.

    For starters, write each sentence on a separate line. Git was written to version control source code, where each line is distinct and has a specific purpose. When you write documents in LaTeX, you often think in terms of paragraphs and write it as a free flowing document. However, in git, changes to a single word in a paragraph get recorded as a change to the entire paragraph.

    One solution is to use git diff --color-words (see my answer to a similar question How to use Mercurial for version control of text documents? where I show an example). However, I must emphasize that splitting into separate lines is a much better option (I only mentioned it in passing in that answer), as I've found it to result in very minimal merge conflicts.

    If you need to look at the code diff, use Git's native diff. To see the difference between two arbitrary commits (versions), you can do so with the shas of each of the commits. See the documentation for more details and also Showing which files have changed between two revisions.

    On the other hand, if you need to look at the diff of your formatted output, use latexdiff which is an excellent utility (written in perl) that takes two latex files and produces a neat diffed output in pdf like this (image source):

    You can combine git and latexdiff (plus latexpand if needed) in a single command using git-latexdiff (e.g. git latexdiff HEAD^ to view the diff between your worktree and the last-but-one commit).

    If you're writing a long document in LaTeX, I'd suggest splitting different chapters into their own files and call them in the main file using the \include{file} command. This way it is easier for you to edit a localized part of your work, and it is also easier for version control, as you know what changes have been made to each chapter, instead of having to figure it out from the logs of one big file.

Using Git efficiently:

    Use branches!. There is perhaps no better advice I can give. I've found branches to be very helpful to keep track of "different ideas" for the text or for "different states" of the work. The master branch should be your main body of work, in its most current "ready to publish" state i.e., if of all the branches, if there is one that you are willing to put your name on it, it should be the master branch.

    Branches are also extremely helpful if you are a graduate student. As any grad student will attest, the advisor is bound to have numerous corrections, most of which you don't agree with. Yet, you might be expected to atleast change them for the time being, even if they are reverted later after discussions. So in such cases, you could create a new branch advisor and make changes to their liking, at the same time maintaining your own development branch. You can then merge the two and cherry pick what you need.

    I would also suggest splitting each section into a different branch and focus only the section corresponding to the branch that you're on. Spawn a branch when you create a new section or dummy sections when you make your initial commit (your choice, really). Resist the urge to edit a different section (say, 3) when you're not on its branch. If you need to edit, commit this one and then checkout the other before branching. I find this very helpful because it keeps the history of the section in its own branch and also tells you at a glance (from the tree) how old some section is. Perhaps you've added material to section 3 that requires tweaking to section 5… Of course, these will, in all probability, be observed during a careful reading, but I find it helpful to see this at a glance so that I can shift gears if I'm getting bored of a section.

    Here's an example of my branches and merges from a recent paper (I use SourceTree on OS X and Git from the command line on Linux). You'll probably notice that I'm not the world's most frequent committer nor do I leave useful comments all the time, but that's no reason for you not to follow those good habits. The main takeaway message is that working in branches is helpful. My thoughts, ideas and development proceeds non-linearly, but I can keep track of them via branches and merge them when I'm satisfied (I also had other branches that led nowhere that were later deleted). I can also "tag" commits if they mean something (e.g., initial submissions to journals/revised submissions/etc.). Here, I've tagged it "version 1", which is where the draft is as of now. The tree represents a week's worth of work.

    Another useful thing to do would be to make document wide changes (such as changing \alpha to \beta everywhere) commits on their own. That way, you can revert changes without having to rollback something else along with it (there are ways you can do this using git, but hey, if your can avoid it, then why not?). The same goes for additions to the preamble.

    Use a remote repo and push your changes upstream regularly. With free service providers like GitHub and Bitbucket (both allow you to create private repos with a free account), there is no reason to not be using these if you're working with Git/Mercurial. At the very least, consider it as a secondary backup (I hope you have a primary one!) for your LaTeX files and a service that allows you to continue editing from where you left on a different machine.

